package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    static final String APIBaseURL = "/apitechu/v1";

    @GetMapping(APIBaseURL + "/products")
    public ArrayList<ProductModel> getProducts(){
        System.out.println("getProducts");

        return ApitechuApplication.productModels;
    }

    @GetMapping(APIBaseURL + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("La Id del producto a obtener es " + id);

        ProductModel result = new ProductModel();

        for (ProductModel product : ApitechuApplication.productModels) {
            if (product.getId().equals(id)) {

                System.out.println("Producto encontrado");
                result = product;
            }
        }

        return result;
    }

    @PostMapping(APIBaseURL + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct){
        System.out.println("createProduct");
        System.out.println("La id del producto a crear es " + newProduct.getId());
        System.out.println("La descripción del producto a crear es " + newProduct.getDesc());
        System.out.println("El precio del producto a crear es " + newProduct.getPrice());

        ApitechuApplication.productModels.add(newProduct);

        return newProduct;
    }

    @PutMapping(APIBaseURL + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("La id del producto a actualizar en URL es " + id);
        System.out.println("La id del producto a actualizar en body es " + product.getId());
        System.out.println("La descripción del producto a actualizar es " + product.getDesc());
        System.out.println("El precio del producto a actualizar es " + product.getPrice());

        for (ProductModel productInList : ApitechuApplication.productModels) {
            productInList.setId(product.getId());
            productInList.setDesc(product.getDesc());
            productInList.setPrice(product.getPrice());
        }

        return product;
    }

    @PatchMapping(APIBaseURL + "/products/{id}")
    public ProductModel parcProduct(@RequestBody ProductModel product, @PathVariable String id) {

        System.out.println("parcProduct");
        System.out.println("Objeto recibido " + product);

        if (!product.getDesc().equals(null)) {
           getProductById(id).setDesc(product.getDesc());
        }
        if (product.getPrice() > 0) {
            getProductById(id).setPrice(product.getPrice());
        }

        return getProductById(id);
    }

    @DeleteMapping(APIBaseURL + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("La id del producto a borrar es " + id);

        ProductModel result = new ProductModel();
        boolean foundCompany = false;

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                System.out.println("producto encontrado");
                foundCompany = true;
                result = productInList;
            }
        }

        if (foundCompany == true) {
            System.out.println("borro elemento");
            ApitechuApplication.productModels.remove(result);
        }

        return result;
    }

}
